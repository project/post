Summary:

The Post module integrates with the Po.st social sharing service.

Installation:

Install the jquery_colorpicker module, along with the included library, then
install the Post module.  Refer to the jquery_colorpicker module README.txt
file for details.

Usage:

See the admin configuration form for details.
